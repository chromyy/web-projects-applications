# Car Comparison

## Team
*P3220 Нгуен Тиен Дат*

## Description 
 + CarComparison - is a website that allows users to compare different types of cars, refer to the price of car accessories, thereby helping users to decide which car is most suitable for them.

 + Car properties such as accessory prices, engine type, fuel type, fuel tank size, etc. will be compared. Administrators can update new models, adjust prices, update accessories. For anonymous users they can only compare cars, with registered users, they can refer to the actual car prices depending on the country they are registered.

## Details 
 + The product is a web application with a database, the user interface is web pages.
 + Technology stack - Spring MVC, MySQL, Thymeleaf.
